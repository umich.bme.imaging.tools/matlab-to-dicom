# Matlab to DICOM image conversion

Write 3D image matrix of size [nx ny nz] to stack of nz 2D DICOM images.


## Releases

Current version is v0.1.

If you'd like to contribute changes to this repo, please follow the 'git-flow' branching model: https://nvie.com/posts/a-successful-git-branching-model/


## Example usage

```
>> ims2dicom('test');
```

This creates DICOM files in the ./test/ subfolder, that can (hopefully) be viewed
with any DICOM viewer (e.g., Amide in Linux).
