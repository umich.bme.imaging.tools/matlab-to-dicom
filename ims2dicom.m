function ims2dicom(ims, voxDims, FOV, dicomInfoProto, varargin)
% function ims2dicom(ims, voxDims, FOV, dicomInfoProto, varargin)
%
% Write 3D image volume to stack of 2D DICOM files.
%
% The 3D image volume is assumed to consist of contiguous voxels,
% with voxel dimensions voxDims and spatial extent FOV.
%
% Inputs:
%   ims          [nx ny nz], real     Image volume. NB! Values will be rescaled and converted to int16.
%                                     Slice 1 corresponds to top of brain.
%   voxDims      [xres yres zres]     1x3 vector with voxel dimensions (cm). (zres = center-to-center spacing between slices)
%   FOV          [xfov yfov zfov]     1x3 vector with field of view (cm)
%   dicomInfoProto                    'Starter', or prototype, dicom info struct.
%                                     This function overwrites some of the entries in this struct.
%                                     See dicominfo(), and diconInfoProto.mat (in this folder).
% Options:
%   outputDir           folder/path string      Where to write the .dcm file(s). Default: 'out'
%   PatientID           string
%   StudyID             string
%   SeriesDescription   string                  DICOM SeriesDescription field. Default: 'custom scan'
%   SeriesNumber        integer
%   tablePos            integer                 Table position (mm). Equal to 'scancent' field in Pfile header (see toppe.utils.loadhdr)
%   rotmat              [3 3]                   Rotation matrix. Default: rotmat = eye(3) = [1 0 0; 0 1 0; 0 0 1]
%
% Output:
%   ./out/*.dcm    DICOM files (one for each slice)
%
% Example:
%   voxDims = [0.2 0.2 0.5];
%   FOV = [12 12 6];
%   load dicomInfoProto;      % load 'di' struct
%   di.PatientAge = '045Y';   % change one of the DICOM header fields by hand
%   ims2dicom(ims, voxDims, FOV, di, 'outputDir', '~/myDicomDir', 'SeriesDescription', 'ihmt', 'SeriesNumber', 20);

% Correspondence between DICOM orientation and GE scanner orientation (I think)
% DICOM             GE            Patient
% increasing x      toward L      toward patient left
% increasing y      toward P      toward patient posterior
% increasing z      toward S      toward patient top (head)

if strcmp(ims, 'test')
	sub_test;
	return;
end

% parse input options
arg.outputDir = 'out';
arg.PatientID = 'anonymous';
arg.StudyID = '0000';
arg.SeriesDescription = 'custom scan';
arg.SeriesNumber = 99;                     % a fun number
arg.tablePos = 0;                          % mm
arg.rotmat = eye(3);
arg = vararg_pair(arg, varargin);

% convert image to int16 (signed)
ims = ims/max(abs(ims(:)))*2^15;
ims = int16(round(ims));

% 'prototype' dicom info (metadata/header)
di = dicomInfoProto;

% study info
di.PatientID         = arg.PatientID;
di.StudyID           = arg.StudyID;
di.SeriesDescription = arg.SeriesDescription;
di.SeriesNumber      = arg.SeriesNumber;

% matrix size
[nx ny nz] = size(ims);
di.Columns = nx;
di.Rows    = ny;
di.ImagesInAcquisition = nz;    % each slice is written to a separate .dcm file

% are these necessary?
di.AcquisitionMatrix(1) = nx;
di.AcquisitionMatrix(2) = ny;
di.AcquisitionMatrix(3) = 0;    % since we're writing each 2D image to a separate file?
di.AcquisitionMatrix(4) = 0;

% voxel size
di.PixelSpacing(1) = voxDims(2)*10;    % mm   Row spacing
di.PixelSpacing(2) = voxDims(1)*10;    % mm   Column spacing
di.SliceThickness  = voxDims(3)*10;    % mm

% FOV (check that it matches matrix and voxel size)
di.ReconstructionDiameter = FOV(1)*10;    % mm

% slice center separation, or gap between slices?
di.SpacingBetweenSlices = di.SliceThickness;

% image orientation (direction cosines)
rowDir = arg.rotmat*[1 0 0]';    % direction of a row
colDir = arg.rotmat*[0 1 0]';    % direction of a column
di.ImageOrientationPatient = [rowDir; colDir];

% loop over slices. Create a .dcm file for each 2D image.
system(sprintf('mkdir -p %s', arg.outputDir));
for s = 1:nz   % goes from top of head, toward I 

	% position (offset from iso-center) of the upper left-hand corner of (2D) image
	di.ImagePositionPatient(1) = (-nx/2+0.5)*voxDims(1)*10;   %. (-FOV(1)/2+voxDims(1)/2)*10;    % mm
	di.ImagePositionPatient(2) = (-ny/2+0.5)*voxDims(2)*10;   %. (-FOV(1)/2+voxDims(1)/2)*10;    % mm
	z = (nz/2+0.5-s)*di.SliceThickness + arg.tablePos;        % mm
	di.ImagePositionPatient(3) = z;
	di.SliceLocation = z;

	% write DICOM file
	fname = sprintf('%s/%d.dcm', arg.outputDir, s);
	dicomwrite(ims(:,:,s)', fname, di);   % NB! Note transpose
end

return;


function sub_test

% create 3d test image 
n = 64;
nz = 10;
voxDims = [0.1 0.1 0.2];                  % voxel size (cm)
FOV = [[n n nz].*voxDims];            % field of view (cm)
p = phantom('Modified Shepp-Logan', n);
w = sin(2*pi*[1:nz]/(2*nz)).^2;              % scale slices to make it a bit more interesting
w = reshape(w, [1 1 nz]);
ims = bsxfun(@times, p, w);

% write to DICOM
load dicomInfoProto                       % di
ims2dicom(ims, voxDims, FOV, di, 'outputDir', 'test', 'SeriesDescription', 'test session', 'SeriesNumber', 99);

return


